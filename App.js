import React, { useState } from 'react';
import { StyleSheet, View, Button, FlatList } from 'react-native';

import TodoItem from './components/TodoItem';
import TodoInput from './components/TodoInput';

export default function App() {
  const [listTodo, setListTodo] = useState([]);
  const [ isModal, setIsModal ] = useState(false);

  const addTodoHandler = enteredTodo => {
    setListTodo(listTodo => [...listTodo, {id: Math.random().toString(), value: enteredTodo}]);
    setIsModal(false);
  };

  const deleteTodoHandler = todoId => {
    setListTodo(currentTodo =>{
      return currentTodo.filter((todo) => todo.id !== todoId)
    });
  };

  const onCancelHandler = () => {
    setIsModal(false);
  }

  return (
    <View style={styles.main}>
      <Button title="Add a New Todo" onPress={() => setIsModal(true)} />
      <TodoInput visible={isModal} onAddTodo={addTodoHandler} onCancel={onCancelHandler} />

      <FlatList
        keyExtractor={(item, index) => item.id} 
        data={listTodo}
        renderItem={itemData => <TodoItem onDelete={deleteTodoHandler} id={itemData.item.id} title={itemData.item.value}/>}/>
    </View>
  );
}

const styles = StyleSheet.create({
  main: {
    padding: 30
  }
});