import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';

const TodoItem = props => {
  return (
    <TouchableOpacity onPress={props.onDelete.bind(this, props.id)}>
      <View style={styles.list}>
        <Text>{props.title}</Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  list: {
    padding: 10,
    marginVertical: 10,
    backgroundColor: '#ccc',
    borderTopColor: '#000',
    borderWidth: 1
  }
});

export default TodoItem;