import React, { useState } from 'react';
import { View, TextInput, Button, StyleSheet, Modal } from 'react-native';

const TodoInput = props => {
  const [enteredTodo, setEnteredTodo] = useState('');


  const todoHandler = (e) => {
    setEnteredTodo(e);
  };

  const addHandler = () => {
    props.onAddTodo(enteredTodo);
    setEnteredTodo('');
  };

  return (
    <Modal visible={props.visible} animationType='slide'>
      <View style={styles.inputContainer}>
        <TextInput placeholder="TODO" style={styles.inputText} onChangeText={todoHandler} value={enteredTodo} />
        <View style={styles.buttonContainer}>
          <View style={styles.button}>
            <Button title="Cancel" color="red" onPress={props.onCancel} />
          </View>
          <View style={styles.button}>
            <Button title="Add" onPress={addHandler} />
          </View>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  inputContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  inputText: {
    borderBottomColor: '#000',
    borderWidth: 1,
    padding: 10,
    width: 200,
    marginBottom: 10
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '50%',
  },
  button: {
    width: '40%'
  }
});

export default TodoInput;